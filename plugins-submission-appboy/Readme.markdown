# Corona Plugin Submission Appboy

* `require "plugin.appboy"`  You should require this plugin before you can use.

* `init(listener , "APIKEY")` This function takes two parameters `listener` is a function that returns an event
    * `event.name` Is the name of the event
    * `event.message` Is the message of the event
    And `APIKEY` is required to connect to appboy server .
    
* `logCustomEvent("EVENT_NAME")` This function takes a single parameter `EVENT_NAME` is the name of the event.

    



