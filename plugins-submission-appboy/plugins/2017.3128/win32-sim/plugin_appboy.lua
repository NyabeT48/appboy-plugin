-- Appboy plugin

local Library = require "CoronaLibrary"

-- Create library
local lib = Library:new{ name='plugin.appboy', publisherId='com.yogome', version = 1 }

-------------------------------------------------------------------------------
-- BEGIN
-------------------------------------------------------------------------------

-- This sample implements the following Lua:
--
--    local PLUGIN_NAME = require "plugin_PLUGIN_NAME"
--    PLUGIN_NAME:showPopup()
--
local fListener

local function showWarning(functionName)
    print( functionName .. "WARNING: The Appboy plugin is only supported on Android devices. Please build for device" );
end

function lib.getAttributionData()
    showWarning("appboy.getAttributionData()")
end

function lib.init(listener , apiKey)
    local event = {
        name = "appboyName",
        message = "Init: The Appboy plugin is only supported on Android devices. Please build for device."
    }
    fListener = listener
    fListener(event)
    showWarning("appboy.init()")
end

function lib.logEvent()
    showWarning("appboy.logEvent()")
end

function lib.logCustomEvent() -- for backwards compatibility only (use logEvent)
    local event = {
            name = "appboyName",
            message = "event_false"
        } 
    fListener(event)
    showWarning("appboy.logCustomEvent()")
end
-------------------------------------------------------------------------------
-- END
-------------------------------------------------------------------------------

-- Return an instance
return lib
