local widget = require "widget"
local appboy = require "plugin.appboy"

local SCREEN_W = display.contentWidth * 2
local SCREEN_H = display.contentHeight * 2
local SCREEN_CENTER_X = display.contentCenterX
local SCREEN_CENTER_Y = display.contentCenterY

local ansInit ,ansEvent

local function handleButtonEvent( event )
    if ( "ended" == event.phase ) then
        appboy.logCustomEvent("CustomEvent_4.0")
        timer.performWithDelay( 3000, function()
             ansEvent.text = "Waiting..."
             ansEvent:setFillColor(0,0.3,0.8)
        end)
    end
end

local function setUI()
    local background = display.newRect( 0, 0, SCREEN_W, SCREEN_H )
    background:setFillColor(0.8)

    local title = display.newText( "APPBOY PLUGIN",  SCREEN_CENTER_X , SCREEN_CENTER_Y * 0.45 , native.systemFont, 35 )
    title:setFillColor( 0, 0, 0 )

    local options = 
    {
        text = "Waiting...",     
        x = SCREEN_CENTER_X,
        width = SCREEN_W * 0.4,
        font = native.systemFont,   
        fontSize = 20,
        align = "center"  -- Alignment parameter
    }

    ansInit = display.newText( options )
    ansInit.y = SCREEN_CENTER_Y * 0.7
    ansInit:setFillColor( 0, 0, 0.9 )

    ansEvent = display.newText( options )
    ansEvent.y = SCREEN_CENTER_Y * 0.9
    ansEvent:setFillColor( 0, 0.3, 0.8 )

    local btnSendEvent = widget.newButton(
        {
            label = "       Send Event\n(CustomEvent_4.0)",
            labelAlign = "center",
            onEvent = handleButtonEvent,
            emboss = false,
            fontSize = 25,
            labelColor = { default={ 1, 1, 1 }, over={ 1, 1, 1 } },
            shape = "roundedRect",
            width = SCREEN_W * 0.3,
            height = SCREEN_H * 0.1,
            cornerRadius = 5,
            fillColor = { default={ 0.2, 0.6, 1}, over={ 1, 0.2, 0.5 } },
        }
    )
    btnSendEvent.x = SCREEN_CENTER_X
    btnSendEvent.y = SCREEN_CENTER_Y * 1.3
end

local function listener( event )
	print( "Received event from Library plugin (" .. event.name .. "): ", event.message )
    if event.message == "init_true" then
        print("------> Correct Configuration Appboy")
        ansInit.text = "Correct Configuration of Appboy"
        ansInit:setFillColor(0,1,0)
    elseif event.message == "init_false" then
        print("------> Error init Appboy")
        ansInit.text = event.message
        ansInit:setFillColor(1,0,0)
    elseif event.message == "event_true" then
         print("------> Success event Appboy")
        ansEvent.text = "Success event Appboy"
        ansEvent:setFillColor(0,1,0)
    elseif event.message == "event_false" then
         print("------> Error event Appboy")
        ansEvent.text = "Custom Event: This Appboy function is only supported on Android devices."
        ansEvent:setFillColor(1,0,0)
    else
        print("------> Error Appboy")
        ansInit.text = event.message
        ansInit:setFillColor(1,0,0)
    end
end

setUI()
--WARNING YOU REQUIRE AN APPBOY API-KEY FOR TESTING 
--https://www.appboy.com YOU CAN GET HERE!
appboy.init( listener , "YOUR-API-KEY" )

