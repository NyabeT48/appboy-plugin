
// This corresponds to the name of the Lua library,
// e.g. [Lua] require "plugin.library"
package plugin.appboy;

import android.content.res.Resources;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ansca.corona.CoronaActivity;
import com.ansca.corona.CoronaEnvironment;
import com.ansca.corona.CoronaLua;
import com.ansca.corona.CoronaRuntime;
import com.ansca.corona.CoronaRuntimeListener;
import com.ansca.corona.CoronaRuntimeTask;
import com.ansca.corona.CoronaRuntimeTaskDispatcher;
import com.appboy.Appboy;
import com.appboy.AppboyLifecycleCallbackListener;
import com.appboy.configuration.AppboyConfig;
import com.naef.jnlua.JavaFunction;
import com.naef.jnlua.LuaState;
import com.naef.jnlua.NamedJavaFunction;

import java.util.ArrayList;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

@SuppressWarnings("WeakerAccess")
public class LuaLoader implements JavaFunction, CoronaRuntimeListener {

	private int fListener;
	private static final String EVENT_NAME = "plugin_appboy_event";

	@SuppressWarnings("unused")
	public LuaLoader() {
		// Initialize member variables.
		fListener = CoronaLua.REFNIL;
		CoronaEnvironment.addRuntimeListener(this);
	}

	@Override
	public int invoke(LuaState L) {
		NamedJavaFunction[] luaFunctions = new NamedJavaFunction[] {
				new InitWrapper(),
				new LogCustomWrapper(),
		};
		String libName = L.toString( 1 );
		L.register(libName, luaFunctions);

		return 1;
	}

	@Override
	public void onLoaded(CoronaRuntime runtime) {
	}

	@Override
	public void onStarted(CoronaRuntime runtime) {
	}

	@Override
	public void onSuspended(CoronaRuntime runtime) {
	}

	@Override
	public void onResumed(CoronaRuntime runtime) {
	}

	@Override
	public void onExiting(CoronaRuntime runtime) {
		CoronaLua.deleteRef( runtime.getLuaState(), fListener );
		fListener = CoronaLua.REFNIL;
	}

	@SuppressWarnings("unused")
	public void dispatchEvent(final String message) {
		CoronaEnvironment.getCoronaActivity().getRuntimeTaskDispatcher().send( new CoronaRuntimeTask() {
			@Override
			public void executeUsing(CoronaRuntime runtime) {
				LuaState L = runtime.getLuaState();

				CoronaLua.newEvent( L, EVENT_NAME );

				L.pushString(message);
				L.setField(-2, "message");

				try {
					CoronaLua.dispatchEvent( L, fListener, 0 );
				} catch (Exception ignored) {
				}
			}
		} );
	}

	@SuppressWarnings({"WeakerAccess", "SameReturnValue"})
	public int init(LuaState L) {
		int listenerIndex = 1;

		if ( CoronaLua.isListener( L, listenerIndex, EVENT_NAME ) ) {
			fListener = CoronaLua.newRef( L, listenerIndex );
		}

		String apiKey = L.checkString( 2 );
		if ( null == apiKey ) {
			System.out.print("------------->AppBoy Error the apiKey is null:  "+ apiKey);
		}else{
			System.out.print("------------->Appboy Init: " + apiKey);
			CoronaActivity activity = CoronaEnvironment.getCoronaActivity();
			configureAppboyAtRuntime(apiKey);
			activity.getApplication().registerActivityLifecycleCallbacks(new AppboyLifecycleCallbackListener());
		}

		return 0;
	}

	public int logCustomEvent(LuaState L) {

		CoronaActivity activity = CoronaEnvironment.getCoronaActivity();
		if (activity == null) {
			return 0;
		}

		String event = L.checkString( 1 );
		if ( null == event ) {
			System.out.print("------------->AppBoy Error the event is null");
		}else
		{
			System.out.print("------------->AppBoy logCustomEvent:  " + event);
           if ( Appboy.getInstance(activity).logCustomEvent(event) ){
               dispatchEvent( "event_true" );
           }else{
               dispatchEvent( "event_false" );
           }
		}

		return 0;
	}

	private void configureAppboyAtRuntime(String apikey) {

		CoronaActivity activity = CoronaEnvironment.getCoronaActivity();

		List<String> localeToApiKeyMapping = new ArrayList<>();
		localeToApiKeyMapping.add("customLocale, customApiKeyForThatLocale");
		localeToApiKeyMapping.add("fr_NC, anotherAPIKey");

		//Resources resources = getResources();
		AppboyConfig appboyConfig = new AppboyConfig.Builder()
				.setApiKey(apikey)
				//.setGcmMessagingRegistrationEnabled(false)
				//.setAdmMessagingRegistrationEnabled(false)
				//.setDisableUilImageCache(true)
				//.setFrescoLibraryEnabled(false)
				//.setSessionTimeout(11)
				//.setHandlePushDeepLinksAutomatically(true)
				//.setSmallNotificationIcon(resources.getResourceEntryName(R.drawable.ic_launcher_hello_appboy))
				//.setLargeNotificationIcon(resources.getResourceEntryName(R.drawable.ic_launcher_hello_appboy))
				//.setTriggerActionMinimumTimeIntervalSeconds(5)
				//.setEnableBackgroundLocationCollection(false)
				//.setDisableLocationCollection(true)
				//.setLocationUpdateDistance(100)
				//.setNewsfeedVisualIndicatorOn(true)
				//.setDefaultNotificationAccentColor(0xFFf33e3e)
				//.setLocaleToApiMapping(localeToApiKeyMapping)
				//.setBadNetworkDataFlushInterval(120)
				//.setGoodNetworkDataFlushInterval(60)
				//.setGreatNetworkDataFlushInterval(10)
				.build();
        if ( Appboy.configure(activity.getApplicationContext(), appboyConfig) ){
            dispatchEvent("init_true");
        }else{
            dispatchEvent("init_false");
        }
	}

	@SuppressWarnings("unused")
	private class InitWrapper implements NamedJavaFunction {

		@Override
		public String getName() {
			return "init";
		}

		@Override
		public int invoke(LuaState L) {
			return init(L);
		}
	}

	private class LogCustomWrapper implements NamedJavaFunction {

		@Override
		public String getName() {
			return "logCustomEvent";
		}

		@Override
		public int invoke(LuaState L) {
			return logCustomEvent(L);
		}
	}
}
